import os
from tornado.options import define

settings = {
    'blog_title': u"Simpals Blog",
    'page_size': 10,
    'template_path': os.path.join(os.path.dirname(__file__), "templates"),
    'static_path': os.path.join(os.path.dirname(__file__), "static"),
    'cookie_secret': "da39a3ee5e6b4b0d3255bfef95601890afd80709",
    'login_url': "/auth/login/",
    'debug': True}

define("port", default=8000, help="run on the given port", type=int)
define("mysql_host", default="127.0.0.1:3306", help="blog database host")
define("mysql_database", default="simpals_blog", help="blog database name")
define("mysql_user", default="simpals", help="blog database user")
define("mysql_password", default="simpals", help="blog database password")
