# coding=utf-8
from tornado.web import RequestHandler


class BaseHandler(RequestHandler):
    """some common things are there"""
    @property
    def db(self):
        return self.application.db

    def get_current_user(self):
        user_id = self.get_secure_cookie("user_id")
        if not user_id:
            return None
        return self.db.get("SELECT * FROM users WHERE id = %s", int(user_id))

    def render(self, *args, **kwargs):
        kwargs['user'] = self.get_secure_cookie("user")
        return super(BaseHandler, self).render(*args, **kwargs)

    # pagination
    def paginator(self, page=1, count=1):
        # checks
        if not isinstance(page, int):
            page = 1
        elif page < 1:
            page = 1
        # list of pages
        pages = [i for i in range(1, count/self.settings['page_size'] + 1)]
        # check if page is not in range
        if page not in pages:
            page = pages[-1]

        prev_page = False if page-1 <= 0 else page-1
        next_page = False if page+1 not in pages else page+1

        # start for db limit
        limit_start = (int(page)-1)*self.settings['page_size']
        # return parameters for db select with limit
        return {'pages': pages,
                'limit_start': limit_start,
                'page_size': self.settings['page_size'],
                'prev_page': prev_page,
                'next_page': next_page}