import tornado.ioloop
import tornado.web
from tornado.httpserver import HTTPServer
import torndb
from tornado.options import options
from settings import *
from blog.request_handler import PostListHandler, PostItemHandler, AddBlogPostHandler
from auth.request_handler import AuthLoginHandler, AuthLogoutHandler


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/blog/item/(?P<item_id>\d+)/$", PostItemHandler),
            (r"/blog/add/$", AddBlogPostHandler),
            (r"/auth/login/$", AuthLoginHandler),
            (r"/auth/logout/$", AuthLogoutHandler),
            (r"/", PostListHandler),
        ]
        tornado.web.Application.__init__(self, handlers, **settings)

        self.db = torndb.Connection(
            host=options.mysql_host, database=options.mysql_database,
            user=options.mysql_user, password=options.mysql_password)


if __name__ == "__main__":
    tornado.options.parse_command_line()
    http_server = HTTPServer(Application())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()