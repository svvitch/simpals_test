from tornado.web import HTTPError, authenticated, asynchronous
from base import BaseHandler
from forms import AddBlogPostForm
from datetime import datetime


class PostListHandler(BaseHandler):
    """list of entries"""

    @asynchronous
    def get(self):
        page = int(self.get_argument('page', 1))
        count = int(self.db.query("SELECT COUNT(*) FROM blog_posts")[0]['COUNT(*)'])
        paginator = self.paginator(page=page, count=count)
        items = self.db.query("SELECT * FROM blog_posts ORDER BY published "
                              "DESC LIMIT %s, %s", paginator['limit_start'], paginator['page_size'])
        self.render("blog_list.html",
                    blog_items=items,
                    prev_page=paginator['prev_page'],
                    next_page=paginator['next_page'])


class PostItemHandler(BaseHandler):
    """single entry page"""
    @asynchronous
    def get(self, item_id):
        blog_item = self.db.get("SELECT * FROM blog_posts WHERE id = %s", item_id)
        if not blog_item:
            raise HTTPError(404)
        self.render("blog_item.html", blog_item=blog_item)


class AddBlogPostHandler(BaseHandler):
    """add a blog post"""
    @authenticated
    def get(self):
        form = AddBlogPostForm()
        self.render("blog_add.html", form=form)

    @asynchronous
    @authenticated
    def post(self):
        form = AddBlogPostForm(self.request.arguments)
        data = {'published': datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        if form.validate():
            data.update(form.data)
            # get auth user
            user = self.get_current_user()
            data.update({'user_id': user['id']})
            # save item
            item_id = self.db.insert("INSERT INTO blog_posts (user_id, title, description, content, published)"
                                     "VALUES ('{user_id}','{title}','{description}','{content}','{published}');".format(**data))
            # redirect to item page
            self.redirect("/blog/item/" + str(item_id) + "/")
        self.render("blog_add.html", form=form)