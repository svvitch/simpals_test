from wtforms import validators, fields
from wtforms_tornado import Form


class AddBlogPostForm(Form):
    title = fields.StringField(validators=[validators.InputRequired(),
                                           validators.Length(max=200,
                                                             message="Max length of title must be less than 200 char"),
                                           ],
                               label="Title")
    description = fields.TextAreaField(validators=[validators.InputRequired()],
                                       label="Description")
    content = fields.TextAreaField(validators=[validators.InputRequired()],
                                   label="Content")