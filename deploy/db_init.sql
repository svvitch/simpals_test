CREATE DATABASE IF NOT EXISTS simpals_blog;
GRANT USAGE ON *.* TO simpals@localhost IDENTIFIED BY 'simpals';
GRANT ALL PRIVILEGES ON simpals_blog.* TO simpals@localhost;
FLUSH PRIVILEGES;
USE simpals_blog;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(75) NOT NULL UNIQUE,
	`password` VARCHAR(50) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

DROP TABLE IF EXISTS `blog_posts`;
CREATE TABLE `blog_posts` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`title` VARCHAR(200) NOT NULL,
	`description` LONGTEXT NOT NULL,
	`content` LONGTEXT NOT NULL,
	`published` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `blog_posts_6340c63c` (`user_id`),
	CONSTRAINT `user_id_refs_id_515d998d` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;