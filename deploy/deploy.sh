#!/bin/bash
########### simpals_blog deploy ##############
############## create virtualenv
virtualenv --no-site-packages env;
############## install pip requirements
echo "Instaling pip packages ...";
echo  "...";
. env/bin/activate;
pip install -r deploy/requirements.txt;
############## config db
echo "Creating initial db for project ...";
echo "Insert mysql root password:";
mysql -uroot -p < deploy/db_init.sql;
############## faked data
echo "Creating faked data ...";
. env/bin/activate;
python deploy/generate_fake_data.py;
echo "!!!! DONE !!!!";