import MySQLdb as mdb
import sys
try:
    from faker import Faker
except:
    print('You must install "faker": pip install fake-factory')

try:
    CON = mdb.connect('localhost', 'simpals', 'simpals', 'simpals_blog')
except mdb.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    sys.exit(1)


def demo_user():
    """
    create one demo user
    username: simpals
    password: simpals
    """
    curs = CON.cursor()
    curs.execute("INSERT INTO users(username, password) VALUES('simpals', 'simpals')") # password is left unencrypted
    CON.commit()

def gen_blog_posts():
    rows = input("Insert nr of rows that will be generated: ")
    fake = Faker()
    data = list()
    curs = CON.cursor()
    # get user id
    curs.execute("SELECT * FROM users LIMIT 1")
    user = curs.fetchone()
    for i in range(0, int(rows)):
        data.append((user[0],  # user_id
                     fake.sentence(nb_words=6, variable_nb_words=True),  # title
                     fake.paragraph(nb_sentences=2, variable_nb_sentences=True),  # description
                     fake.paragraph(nb_sentences=50, variable_nb_sentences=True),  # content
                     fake.date_time_between(start_date="-30y", end_date="now")))  # published

    curs.executemany("""INSERT INTO blog_posts (user_id, title, description, content, published) VALUES (%s,%s,%s,%s,%s)""", data)
    CON.commit()


def generate():
    demo_user()
    gen_blog_posts()
    if CON:
        CON.close()

if __name__ == '__main__':
    generate()