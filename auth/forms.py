from wtforms import fields
from wtforms.validators import InputRequired
from wtforms_tornado import Form


class LoginForm(Form):
    username = fields.StringField(validators=[InputRequired()])
    password = fields.PasswordField(validators=[InputRequired()])