from tornado.web import RequestHandler
from tornado.web import asynchronous
from forms import LoginForm
from base import BaseHandler


class AuthLoginHandler(BaseHandler):

    def get(self):
        form = LoginForm()
        self.render("login.html", form=form, errors=list())

    @asynchronous
    def post(self):
        form = LoginForm(self.request.arguments)
        errors = list()
        if form.validate():
            user = self.db.get("SELECT * FROM users WHERE username=%s", form.data['username'])
            if user:
                self.set_secure_cookie("user", user['username'])
                self.set_secure_cookie("user_id", str(user['id']))
                self.redirect("/")
            else:
                errors.append("User does not exist !")
        self.render("login.html", form=form, errors=errors)


class AuthLogoutHandler(RequestHandler):
    def get(self):
        self.clear_cookie("user")
        self.clear_cookie("user_id")
        self.redirect(self.get_argument("next", "/"))